libapache-sessionx-perl (2.01-5) UNRELEASED; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright:
    + migrate pre-1.0 format to 1.0 using "cme fix dpkg-copyright"
    + Use Kanru Chen's @debian.org e-mail address instead of the no more
      working @debian.org.tw one.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Add Brazilian Portuguese debconf templates translation.
    Thanks to Adriano Rafael Gomes. (Closes: #824291)
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

 -- gregor herrmann <gregoa@debian.org>  Thu, 05 Jul 2012 12:13:04 -0600

libapache-sessionx-perl (2.01-4) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * Add Danish debconf translation. Thanks to Joe Dalton (Closes: #657053)
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Mon, 23 Jan 2012 21:20:12 +0100

libapache-sessionx-perl (2.01-3) unstable; urgency=low

  [ Dario Minnucci ]
  * debian/control:
    + Typo in package description fixed. (Closes: #571113) 

  [ Ansgar Burchardt ]
  * Correct an additional spelling error.
    + updated patch: spelling.patch
  * Email change: Ansgar Burchardt -> ansgar@debian.org

  [ gregor herrmann ]
  * Add Japanese debconf translation, thanks to Hideki Yamane
    (closes: #626804).
  * Run debconf-updatepo during clean, build depend on po-debconf.
  * Update patch headers.
  * debian/copyright: update license stanzas.
  * Bump debhelper compatibility level to 8.
  * Add /me to Uploaders.
  * Set Standards-Version to 3.9.2 (no further changes).

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 May 2011 17:19:33 +0200

libapache-sessionx-perl (2.01-2) unstable; urgency=low

  [ Jonathan Yu ]
  * Bringing this package under the pkg-perl group (Closes: #543607)
  * Updated d/watch with CPAN search site
  * Standards-Version 3.8.3
    + Remove version dependency on perl
    + Add Vcs-* and Homepage fields

  [ Ryan Niebur ]
  * Update jawnsy's email address

  [ Dario Minnucci ]
  * Debconf translations:
    - Spanish added. (Closes: #560997)

  [ Ansgar Burchardt ]
  * Refresh rules for debhelper 7.
  * Use source format 3.0 (quilt).
  * Move changes to upstream source to a patch.
    + New patch: debian-config.patch
  * Correct spelling errors pointed out by lintian.
    + New patch: spelling.patch
  * Add lintian override for unused-debconf-template (false positive if
    maintainer scripts are written in Perl).
  * Add lintian override for non-standard-dir-perm for directories below
    /var/lib/libapache-sessionx-perl (they need to be writeable by apache).
  * Use __Choices instead of _Choices in debconf templates.
  * Bump Standards-Version to 3.8.4.
  * Add myself to Uploaders.

  [ gregor herrmann ]
  * debian/copyright: reformat, add additional copyright holders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 31 Jan 2010 22:14:06 +0900

libapache-sessionx-perl (2.01-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix pending l10n issues. Debconf translations:
    - Swedish. Closes: #492998
    - Traditional Chinese. Closes: #503171
    - Russian. Closes: #503405
    - Italian. Closes: #503648
    - Basque. Closes: #503651

 -- Christian Perrier <bubulle@debian.org>  Mon, 27 Oct 2008 20:44:55 +0100

libapache-sessionx-perl (2.01-1) unstable; urgency=low

  * New upstream release.
  * Debconf translations:
    - Portuguese. Closes: #414815
    - German. Closes: #416064
    - Dutch. Closes: #436870
  * Removed the English translation - it was a silly idea to begin with.
  * Update Standards-Version to 3.7.3.  No changes.

 -- Angus Lees <gus@debian.org>  Mon, 24 Dec 2007 01:42:20 +0000

libapache-sessionx-perl (2.00b5-3.2) unstable; urgency=low

  * Non-maintainer upload to fix pending l10n issues.
  * Debconf translations:
    - Czech. Closes: #412576
    - Basque. Closes: #400861

 -- Christian Perrier <bubulle@debian.org>  Tue, 27 Feb 2007 07:07:54 +0100

libapache-sessionx-perl (2.00b5-3.1) unstable; urgency=low

  * Non-maintainer upload to fix longstanding l10n issues
  * Debconf translations:
    - Swedish added. Closes: #333724
    - French updated. Closes: #350902
    - Vietnamese updated. Sent during the call for updates of the NMU campaign.
    - Russian added. Sent during the call for updates of the NMU campaign.
    - Useless English "translation" removed
    - Brazilian Portuguese added. Sent during the call for updates of
      the NMU campaign.
  * Lintian fixes:
    - Use debian/compat for debhelper compatibility and use a default value
      set to 4
    - Rewrite debconf templates to make them compliant with the suggested
      writing style in the Develoepr's Reference
      Closes: #399657

 -- Christian Perrier <bubulle@debian.org>  Mon, 13 Nov 2006 20:05:18 +0100

libapache-sessionx-perl (2.00b5-3) unstable; urgency=low

  * Add vietnamese translation from Clytie Siddall, closes: #312231.
  * Add french translation from Steve, closes: #300348.
  * Add Czech translation from Martin Šín, closes: #319379.
  * Don't mark libapache-sessionx-perl/default_store:Choices as
    translatable.

 -- Angus Lees <gus@debian.org>  Thu, 28 Jul 2005 09:07:48 +1000

libapache-sessionx-perl (2.00b5-2) unstable; urgency=low

  * Change section to "perl".
  * Bug fix: "libapache-sessionx-perl: Please switch to gettext-based
    debconf templates", thanks to Martin Quinson (Closes: #234814).
  * Replace Build-Depends-Indep with Build-Depends, following what I think
    is a correct reading of the relevant (tortuous) policy section..
  * Update Standards-Version to 3.6.1.
  * Modify Makefile.PL and debian/rules to ignore arch (tla) metafiles.

 -- Angus Lees <gus@debian.org>  Sun, 14 Mar 2004 14:28:56 +1100

libapache-sessionx-perl (2.00b5-1) unstable; urgency=low

  * New upstream release.
  * Change dependency from libmd5-perl to libdigest-md5-perl.
  * Make Apache::SessionX::Config a simple wrapper to load the debconf
    generated config, rather than patch Apache::SessionX, etc to load the
    new file themselves.

 -- Angus Lees <gus@debian.org>  Mon, 10 Mar 2003 22:01:50 +1100

libapache-sessionx-perl (2.00b3-2) unstable; urgency=low

  * Forgot to change one instance of _keys -> priv_keys, which broke the
    postinst completely (oops).  (closes: #163052)

 -- Angus Lees <gus@debian.org>  Thu,  3 Oct 2002 11:39:02 +1000

libapache-sessionx-perl (2.00b3-1) unstable; urgency=low

  * Initial Release.

 -- Angus Lees <gus@debian.org>  Fri, 27 Sep 2002 19:05:35 +1000


